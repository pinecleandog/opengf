-- Per client settings for the Addon
OpenGF_Settings = 
{
	["MinimapPos"] = 45 -- Default minimap icon position
}

-- Saved data used by the addon itself
OpenGF_Data = 
{
	Role = 0, -- The role flag that is saved for the next use
	Note = "", -- The user's note is saved for the next use
}

OpenGF = {}

-- All the OpenGF.instances the player is listed in, this is treated as a set.
OpenGF.listings = {}

-- Table containing information about locked instances, indexed by dungeon name.
OpenGF.lockedInstances = {}

OpenGF_MainFrame.CreateWindow()
OpenGF_MainFrame.frame:RegisterEvent("ADDON_LOADED");

ICON_UNLISTED = "Interface\\Icons\\Achievement_boss_cthun"
ICON_LISTED = "Interface\\Icons\\Ability_hunter_aspectoftheviper"

OpenGF_MinimapIcon.Create(ICON_UNLISTED)

-- The currently selected instance on the client's UI
OpenGF.selectedInstance = 0

-- Handles the main events hooked to the OpenGF_MainFrame
function OpenGF.HandleEvent(self, event, arg1)
	if(event == "ADDON_LOADED" and arg1 == "OpenGF") then
		OpenGF.Init()
	end
end
OpenGF_MainFrame.frame:SetScript("OnEvent", OpenGF.HandleEvent);

-- Run when the player enters the world
function OpenGF.Init()
	OpenGF.PrintLoginMessage()
	OpenGF_MainFrame.CreateWidgets()
	OpenGF.UpdateRoleBoxes()
	OpenGF.UpdateRole()
	-- Set the initial positon from either a saved variable, or its default value
	OpenGF_MinimapIcon.Reposition()
	OpenGF_PlayerContextMenu.Create()
	OpenGF_Comms.Init()

	-- Register the main frame as specialf frame, so it can be closed with escape
	table.insert(UISpecialFrames, OpenGF_MainFrame.frame:GetName());
	
	-- Add slash command for opening UI
	SlashCmdList["OGF"] = OpenGF_MainFrame.Toggle
	SLASH_OGF1 = "/ogf"
end


-- Populates OpenGF.lockedInstances with information about the player's current lockouts
function OpenGF.PopulateLockedInstances()
    wipe(OpenGF.lockedInstances)
    for i=1, GetNumSavedInstances() do
		local heroic = false
        local name, _, reset, _, locked, extended, _, isRaid, _, difficultyName, _, _ = GetSavedInstanceInfo(i)
		
        local minutes = reset / 60.0
        local hours = math.floor(minutes / 60.0)
        local days = math.floor(hours / 24.0)
        minutes = math.floor(minutes % 60.0)
        
        local time_message = "" -- shows how much time is remaining on the lockout in day/hour/minute format
        if (days > 0) then
            time_message = days .. " days " .. hours .. " hours " .. minutes .. " mins until lock expires."
        else
            time_message = minutes .. " mins until lock expires."
            if (hours > 0) then
                time_message = hours .. " hours " .. time_message
            end
        end	
		
        if (difficultyName ~= nil) and (string.find(difficultyName,"Heroic")) then
            heroic = true
            name = name .. " Heroic"
        end
		
		local lockout = {
			Message = time_message,
			Reset = reset,
			Heroic = heroic,
		}
        OpenGF.lockedInstances[name] = lockout
    end
end


-- Called when the player's role is updated (when a checkbox is clicked)
function OpenGF.UpdateRole(checkbox)

	-- DPS FLAG
	if(OpenGF_MainFrame.dpsCheck:GetChecked() == 1) then
		OpenGF_Data.Role = setflag(OpenGF_Data.Role, ROLE_FLAG_DPS)
	else
		OpenGF_Data.Role = clearflag(OpenGF_Data.Role, ROLE_FLAG_DPS)
	end

	-- TANK FLAG
	if(OpenGF_MainFrame.tankCheck:GetChecked() == 1) then
		OpenGF_Data.Role = setflag(OpenGF_Data.Role, ROLE_FLAG_TANK)
	else
		OpenGF_Data.Role = clearflag(OpenGF_Data.Role, ROLE_FLAG_TANK)
	end
	
	-- HEALER FLAG
	if(OpenGF_MainFrame.healCheck:GetChecked() == 1) then
		OpenGF_Data.Role = setflag(OpenGF_Data.Role, ROLE_FLAG_HEAL)
	else
		OpenGF_Data.Role = clearflag(OpenGF_Data.Role, ROLE_FLAG_HEAL)
	end
	
	-- No role selected
	if(OpenGF_Data.Role == 0) then
		OpenGF.UnlistAll()
	end
end

-- Update's the player's note, called when the cursor leaves the text area
function OpenGF.UpdateNote()
	OpenGF_Data.Note = OpenGF_MainFrame.note:GetText()
end

-- Sets up role check boxes based on the saved flag
function OpenGF.UpdateRoleBoxes()

	if(OpenGF_Player.CanPerformRole(ROLE_FLAG_TANK) == false) then
		OpenGF_MainFrame.tankCheck:Disable()
		OpenGF_MainFrame.tankTag:SetTextColor(1,1,1,0.5)
		
	end
	
	if(OpenGF_Player.CanPerformRole(ROLE_FLAG_HEAL) == false) then
		OpenGF_MainFrame.healCheck:Disable()
		OpenGF_MainFrame.healTag:SetTextColor(1,1,1,0.5)
	end

	if(testflag(OpenGF_Data.Role, ROLE_FLAG_DPS)) then
		OpenGF_MainFrame.dpsCheck:SetChecked(true)
	end
	
	if(testflag(OpenGF_Data.Role, ROLE_FLAG_TANK) and OpenGF_Player.CanPerformRole(ROLE_FLAG_TANK)) then
		OpenGF_MainFrame.tankCheck:SetChecked(true)
	end
	
	if(testflag(OpenGF_Data.Role, ROLE_FLAG_HEAL) and OpenGF_Player.CanPerformRole(ROLE_FLAG_HEAL)) then
		OpenGF_MainFrame.healCheck:SetChecked(true)
	end
	
end

-- Called when the category dropdown changes
function OpenGF.UpdateCategory(self)
	-- Update the selected item
	UIDropDownMenu_SetSelectedID(OpenGF_MainFrame.categorySelect, self:GetID())
	OpenGF_MainFrame.UpdateDropdowns(self:GetID())
	OpenGF_MainFrame.currentCategory = self:GetID()
	
	-- "Select Instance" is what it will be after category changes, make sure no instance is selected
	OpenGF.selectedInstance = 0
	OpenGF_MainFrame.UpdateListingMethod(OpenGF.selectedInstance)
	OpenGF_MainFrame.UpdateSearchButton()
	OpenGF_MainFrame.ResetListedPlayers()
end

-- Called when one of the instance dropdowns change. Offset is the amount of OpenGF.instances prior to the current category
function OpenGF.UpdateInstance(self, selector, offset)
	UIDropDownMenu_SetSelectedID(selector, self:GetID())
	OpenGF.selectedInstance = OpenGF.instances[self:GetID() + (offset - 1)].id
	
	OpenGF_MainFrame.UpdateSearchButton()
	OpenGF_MainFrame.UpdateListingMethod(OpenGF.selectedInstance)
	OpenGF_MainFrame.ResetListedPlayers()
end


-- Search for other players listed for OpenGF.selectedInstance
function OpenGF.SearchFunction()
	if(OpenGF.selectedInstance ~= nil and OpenGF.selectedInstance ~= 0) then
		OpenGF_MainFrame.ResetListedPlayers()
		OpenGF_Comms.Broadcast(OpenGF.selectedInstance, OpenGF_MainFrame.GetCategoryIdentifier())	
	end
end

-- Removes all listed OpenGF.instances from the set
function OpenGF.UnlistAll()
	for k,v in pairs (OpenGF.listings) do
		OpenGF.listings[k] = nil
	end
	-- Make sure the right buttons are displayed
	OpenGF_MainFrame.UpdateListingMethod(OpenGF.selectedInstance)
	OpenGF_MinimapIcon.UpdateIcon()
end

-- List the player for the OpenGF.selectedInstance
function OpenGF.EnlistFunction()

	if(	OpenGF_Data.Role == 0) then
		print("|cffffff00You must select a role before listing yourself")
		return
	end

	
	if(OpenGF.selectedInstance ~= 0 and OpenGF.selectedInstance ~= nil) then
		if(OpenGF_Util.ListContains(OpenGF.listings, OpenGF.selectedInstance) == false) then
		
			local plyLevel = UnitLevel("Player")
			local ilevel = OpenGF_Player.GetAverageIlevel();
				
			if(plyLevel < OpenGF.instances[OpenGF.selectedInstance].minLevel) then
				print("|cffff0000Your level is not high enough for that instance");
				return
			end
			
			if(plyLevel > OpenGF.instances[OpenGF.selectedInstance].maxLevel) then
				print("|cffff0000Your level is too high for that instance");
				return
			end
			
			if(ilevel < OpenGF.instances[OpenGF.selectedInstance].ilevel) then
				print("|cffff0000Your average item level is too low for this instance ("..ilevel.."/"..OpenGF.instances[OpenGF.selectedInstance].ilevel..")");
				return
			end
		
			table.insert(OpenGF.listings, OpenGF.selectedInstance)
			OpenGF_MainFrame.UpdateListingMethod(OpenGF.selectedInstance)
			OpenGF_MinimapIcon.UpdateIcon()
		else
			print("|cffffff00You are already listed for that instance");
		end
		
	end
end

-- Removed OpenGF.selectedInstance from the listings
function OpenGF.UnlistFunction()
	if(OpenGF.selectedInstance ~= 0 and OpenGF.selectedInstance ~= nil) then
		local listIndex = OpenGF_Util.GetIndexByValue(OpenGF.listings, OpenGF.selectedInstance)
		if(listIndex >= 0) then
			table.remove(OpenGF.listings, listIndex)
			OpenGF_MainFrame.UpdateListingMethod(OpenGF.selectedInstance)
			OpenGF_MinimapIcon.UpdateIcon()
		end
	end
end

-- Prints stuff to the player once they have entered the world
function OpenGF.PrintLoginMessage()
	print("|cff00ccff Thank you for participating the the OpenGF open beta. Please be aware that some features may not work 100% and there will most likely be bugs. Please PM Pinecleandog on the forums if you would like to report a bug or suggest a feature.")
end

