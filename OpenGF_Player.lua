OpenGF_Player = {}

OpenGF_Player.numGroupMembers = 0
-- if the group was created through the UI
OpenGF_Player.isDungeonGroup = false
OpenGF_Player.dungeonGroupType = nil;

OpenGF_Player.groupListener = CreateFrame("Frame", "ogf_groupListener")
OpenGF_Player.groupListener:RegisterEvent("PARTY_MEMBERS_CHANGED")

-- Returns a classID listed in OpenGF_DB.lua, depending on the player's class, returns 0 if the class is invalid
function OpenGF_Player.GetPlayerClassID()
	localizedClass, englishClass= UnitClass("player");
	for k, v in pairs(OpenGF.classes) do
		if (string.upper(v.name) == englishClass) then 
			return v.id
		end
	end
	return 0
end

function OpenGF_Player.CanPerformRole(role)

	local roleCount = table.getn(OpenGF.classRoles[OpenGF_Player.GetPlayerClassID() + 1].roles)
	for i = 0, roleCount, 1 do
		if (OpenGF.classRoles[OpenGF_Player.GetPlayerClassID() + 1].roles[i] == role) then 
			return true
		end
	end
	return false

end

-- Fired when the party members change in a group
function OpenGF_Player.OnGroupChange()
	local currentGroupCount = GetNumPartyMembers()

	-- unlist from all dungeons if you join a group
	OpenGF.UnlistAll()
	-- A new group has been formed
	if( OpenGF_Player.numGroupMembers < 1 and currentGroupCount > 0) then
			-- The group was created through the addon's UI, and you are the leader
			if(OpenGF_Player.isDungeonGroup and IsPartyLeader()) then			
				SetLootMethod("needbeforegreed") 
				if(OpenGF_Player.dungeonGroupType == GROUP_TYPE_DUNGEON) then
					SetDungeonDifficulty(1) -- normal
				elseif(OpenGF_Player.dungeonGroupType == GROUP_TYPE_HEROIC) then
					SetDungeonDifficulty(2) -- heroic
				end
			end
	end
	-- The group has disbanded
	if(currentGroupCount == 0 and OpenGF_Player.numGroupMembers > 0) then
		OpenGF_Player.isDungeonGroup = false;
	end
	
	-- Update member count
	OpenGF_Player.numGroupMembers = currentGroupCount
end
OpenGF_Player.groupListener:SetScript("OnEvent", OpenGF_Player.OnGroupChange)

-- Calculates the player's average iLevel
function OpenGF_Player.GetAverageIlevel()
	local total = 0
	local count = 0
	
	for i = 1, 18, 1 do
	
		-- ignore shirt
		if(i ~= 4) then
			local item = GetInventoryItemLink("player", i)
			
			if(item) then
				local _,_,_, ilevel = GetItemInfo(item)
				total = total + ilevel
			end
			
			-- ignore offhand if not equipt, as not all OpenGF.classes use them
			if(i == 17 and item == nil) then
				--p
			else
				count = count + 1
			end			
		end
	end
	
	if(count > 0) then
		return total / count
	end
	
	return 0
end