OpenGF_GUI = {}

OpenGF_GUI.prefix = "ogf_"

-- Creates a standard OpenGF_GUI frame
function OpenGF_GUI.CreateFrame(name, parent, width, height , movable, template , alignment,  anchor, xPos , yPos, layer)
	
	parent = parent or UIParent
	movable = movable or false
	alignment = alignment or "CENTER"
	anchor = anchor or "CENTER"
	layer = layer or "MEDIUM"
	
	-- Create frame and set it up
	local frame = CreateFrame("Frame",OpenGF_GUI.prefix..name, parent, template)
	
	frame:SetFrameStrata(layer)
	frame:SetWidth(width)
	frame:SetHeight(height)
	
	frame:SetPoint(alignment, parent, anchor, xPos, yPos)
	
	if(movable) then
		frame:SetMovable(true)
		frame:RegisterForDrag("LeftButton")
		frame:SetScript("OnDragStart", frame.StartMoving)
		frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
	end
	
	return frame
	
end

-- Sets a frame's background and border
function OpenGF_GUI.SetFrameStyle(frame, r, g, b, a)
	
	-- Using default UI look for now
	frame:SetBackdrop(
	{
		bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
		edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
		tile = true, tileSize = 16, edgeSize = 16, 
		insets = { left = 4, right = 4, top = 4, bottom = 4 }
	});
	
	frame:SetBackdropColor(r, g, b, a)
	
end

-- Creates special context buttons for context menus
function OpenGF_GUI.CreateContextItem(name, parent, width, height, text, clickFunction, xPos, yPos, alignment, anchor)
	xPos = xPos or 0
	yPos = yPos or 0
	
	alignment = alignment or "CENTER"
	anchor = anchor or "CENTER"
	
	local button = CreateFrame("Button", OpenGF_GUI.prefix..name, parent, "SecureActionButtonTemplate")
	button:SetWidth(width)
	button:SetHeight(height)
	button:SetPoint(alignment, parent, anchor, xPos, yPos)
	
	button.label = OpenGF_GUI.CreateLabel(button, name.."_label", text, 0, 0, "TOPLEFT", "TOPLEFT", "DIALOG")

	-- TEMP
	button:SetHighlightTexture("Interface/Buttons/UI-Panel-Button-Highlight")

	-- Set the function to call when clicked
	if(clickFunction ~= nil) then
		button:SetScript("OnClick", clickFunction)
	end
	
	return button
end

-- Creates a basic button
function OpenGF_GUI.CreateButton(name, parent, width, height, text, clickFunction, xPos, yPos, template, alignment, anchor , layer)
	
	xPos = xPos or 0
	yPos = yPos or 0
	
	alignment = alignment or "CENTER"
	anchor = anchor or "CENTER"
	layer = layer or "DIALOG"
	
	
	local button = CreateFrame("Button", OpenGF_GUI.prefix..name, parent, template)
	button:SetWidth(width)
	button:SetHeight(height)
	button:SetPoint(alignment, parent, anchor, xPos, yPos)
	button:SetText(text)
	
	-- Use default look for buttons for now
	button:SetNormalTexture("Interface/Buttons/UI-Panel-Button-Up")
	button:SetHighlightTexture("Interface/Buttons/UI-Panel-Button-Highlight")
	button:SetPushedTexture("Interface/Buttons/UI-Panel-Button-Down")
	
	-- Set the function to call when clicked
	if(clickFunction ~= nil) then
		button:SetScript("OnClick", clickFunction)
	end
	
	return button
end

-- Creates a row for the player list
function OpenGF_GUI.CreateListRow(name, parent, width, height, xPos, yPos)
	local row = CreateFrame("Button", OpenGF_GUI.prefix..name, parent)
	row:SetWidth(width)
	row:SetHeight(height)
	row:SetPoint("TOPLEFT", parent, "TOPLEFT", xPos, yPos)
	
	-- TEMP
	row:SetHighlightTexture("Interface/Buttons/UI-Panel-Button-Highlight")
	
	-- Create labels
	
	row.level = OpenGF_GUI.CreateLabel(row, OpenGF_GUI.prefix..name.."_level", "", 8, 0, "LEFT", "LEFT")
	row.role= OpenGF_GUI.CreateLabel(row, OpenGF_GUI.prefix..name.."_role", "", 35 , 0, "LEFT", "LEFT")
	
	row.plyName= OpenGF_GUI.CreateLabel(row, OpenGF_GUI.prefix..name.."_plyName", "", 82 , 0, "LEFT", "LEFT")
	
	row.class = OpenGF_GUI.CreateLabel(row, OpenGF_GUI.prefix..name.."_class", "", 225 , 0, "LEFT", "LEFT")
	row.note = OpenGF_GUI.CreateLabel(row, OpenGF_GUI.prefix..name.."_note", "" , 310 , 0, "LEFT", "LEFT")

	return row
end

-- Creates a window at the mouse point with some menu options. Must be hidden/show and position set to mouse position after creation
function OpenGF_GUI.CreateContextMenu(name, width, height)
	--TODO make this scale depending on the amount of options
	local menu = CreateFrame("Frame", OpenGF_GUI.prefix..name, UIParent, nil)
	menu:SetFrameStrata("DIALOG")
	menu:SetWidth(width)
	menu:SetHeight(height)
	
	menu:SetBackdrop(
	{
		bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
		edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
		tile = true, tileSize = 16, edgeSize = 16, 
		insets = { left = 4, right = 4, top = 4, bottom = 4 }
	});
	
	menu:SetBackdropColor(0,0,0,1)
		
	return menu
end

-- Creates all labels and icons for a row, based on input data
function OpenGF_GUI.SetRowData(row, plyLevel, role, plyName, class, note, groupType)
	
	row.level:SetText(tostring(plyLevel))
	
	local roleString = ""
	--TODO replace sting with icons
	
	if(testflag(role, ROLE_FLAG_DPS)) then
		roleString = roleString .. "D "
	end
	
	if(testflag(role, ROLE_FLAG_TANK)) then
		roleString = roleString .. "T "
	end
	
	if(testflag(role, ROLE_FLAG_HEAL)) then
		roleString = roleString .. "H "
	end
	
	row.role:SetText(roleString)
	row.plyName:SetText(plyName)

	row:SetScript("OnClick", function() OpenGF_PlayerContextMenu.Show(plyName, groupType) end)
	
	if(class == nil) then
		class = 0
	end
	
	local className = OpenGF_Util.GetClassName(class)
	
	if(class ~= 0) then
		local classColour = RAID_CLASS_COLORS[string.upper(className)]
		row.class:SetTextColor(classColour.r, classColour.g, classColour.b)
	end

	row.class:SetText(className)
	
	row.note:SetText(note)
end

-- Sets the style for the OpenGF_GUI button. First argument is the button itself, the next three are the normal, hover and pressed image in order
function OpenGF_GUI.SetButtonStyle(button, normTexture, highlightTexture, pushedTexture )
	button:SetNormalTexture(normTexture)
	button:SetHighlightTexture(highlightTexture)
	button:SetPushedTexture(pushedTexture)
end

-- Creates a text label, parents to frame
function OpenGF_GUI.CreateLabel(frame, name, text, xPos, yPos, alignment, anchor, layer)

	alignment = alignment or "TOPLEFT"
	anchor = anchor or "TOPLEFT"
	layer = layer or "OVERLAY"

	local label = frame:CreateFontString(OpenGF_GUI.prefix..name, layer,"GameTooltipText")
	label:SetPoint(alignment, frame, anchor, xPos, yPos)
	label:SetText(text)
	
	return label
end

-- Creates a checkbox which calls onClick when the checked state changes
function OpenGF_GUI.CreateCheckbox(name, parent, width, height, onClick, xPos, yPos, alignment, anchor)
	
	xPos = xPos or 0
	yPos = yPos or 0
	alignment = alignment or "TOPLEFT"
	anchor = anchor or "TOPLEFT"
	
	local checkbox = CreateFrame("CheckButton", OpenGF_GUI.prefix..name, parent, "UICheckButtonTemplate")
	checkbox:SetWidth(width)
	checkbox:SetHeight(height)
	checkbox:SetPoint(alignment, parent, anchor, xPos, yPos)
	
	if(onClick ~= nil) then
		checkbox:SetScript("OnClick",onClick)
	end
	
	return checkbox
end

-- Creates an editable text field the player can enter text to
function OpenGF_GUI.CreateTextField(name, parent, width, height, autofocus, xPos, yPos, alignment, anchor)
	
	autofocus = autofocus or false
	xPos = xPos or 0
	yPos = yPos or 0
	alignment = alignment or "TOPLEFT"
	anchor = anchor or "TOPLEFT"
	
	local textfield = CreateFrame("EditBox", OpenGF_GUI.prefix..name, parent, "InputBoxTemplate")
	textfield:SetFontObject(GameFontNormal)
	textfield:SetWidth(width)
	textfield:SetHeight(height)
	textfield:SetPoint(alignment, parent, anchor, xPos, yPos)
	textfield:SetAutoFocus(autofocus)
	
	if(autofocus == false) then
		textfield:ClearFocus()
	end
	
	return textfield
	
end

-- Inits a dropdown, using the data from list - from the index startP to endP. Offset is used if you want to be able to track the index of the list item
function OpenGF_GUI.InitDropdown(list, startP, endP, onSelect, selector, offset)
	OpenGF.PopulateLockedInstances()
	local info = UIDropDownMenu_CreateInfo()
	for i = startP, endP, 1
	do
		info = UIDropDownMenu_CreateInfo()
		info.text = list[i].name
		info.value = list[i].name

		if(list == OpenGF.instances) then
			if(list[i].ilevel > 0) then -- show min iLevel for OpenGF.instances
				info.text = "("..list[i].ilevel.." iLvl) "..info.text
			else -- non iLevel required OpenGF.instances show level
				info.text = "(".. list[i].minLevel.."-"..list[i].maxLevel..") "..info.text
			end
			--colored text for lockout
			if(OpenGF.lockedInstances[list[i].name] ~= nil) then
				info.text = "|cFF999999" .. info.text .. "|r|cFFFF0000 (SAVED)|r |cFFFFFF00" .. OpenGF.lockedInstances[list[i].name].Message .. "|r"
			end
		end
		
		info.func = function() onSelect(this, selector, offset) end
		UIDropDownMenu_AddButton(info, level)
	end
end

-- Creates a dropdown, using the information in list. Starting at index startP to endP inclusive. onSelect is the function that is called when a dropdown item is selected. defaultVal is what is displayed at the 0th selection
function OpenGF_GUI.CreateDropdown(name, parent, width, list, startP, endP, onSelect, defaultVal, selected, xPos, yPos, alignment, anchor)
	
	xPos = xPos or 0
	yPos = yPos or 0
	alignment = alignment or "TOPLEFT"
	anchor = anchor or "TOPLEFT"
	
	local selector = CreateFrame("Frame", OpenGF_GUI.prefix..name, parent, "UIDropDownMenuTemplate")
	selector:SetPoint(alignment, parent, anchor, xPos, yPos)
	
	-- the offset in the list, can be used later to find the correct element in the list again
	local offset = startP
	
	UIDropDownMenu_Initialize(selector, function() OpenGF_GUI.InitDropdown(list, startP, endP, onSelect, selector, offset) end)
	
	UIDropDownMenu_SetWidth(selector, width)
	UIDropDownMenu_SetText(selector, defaultVal )
	UIDropDownMenu_SetSelectedID(selector, selected)
	UIDropDownMenu_JustifyText(selector, "LEFT")
	
	return selector
	
end

