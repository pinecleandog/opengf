OpenGF_Util = {}

-- Bit Flags:

-- Sets a flag
function setflag(set, flag)
  if set % (2*flag) >= flag then
    return set
  end
  return set + flag
end

-- Clears a flag
function clearflag(set, flag) -- clear flag
  if set % (2*flag) >= flag then
    return set - flag
  end
  return set
end

-- Returns true if a flag is set
function testflag(set, flag)
  return set % (2*flag) >= flag
end

-- Returns true if "list" contains "val"
function OpenGF_Util.ListContains(list, val)
	for k, v in pairs(list) do
		if (v== val) then 
			return true
		end
	end
	return false
end

-- Returns the size of a table
function OpenGF_Util.GetListLength(list)
	local count = 0
	
	for _ in pairs(list) do
		count = count + 1 
	end
	
	return count
end

-- Returns the the first index the value was found, if the value was not found, returns -1
function OpenGF_Util.GetIndexByValue(list, val)
	local i = 1
	for k, v in pairs(list) do
		if (v == val) then 
			return i
		end
		i = i + 1
	end
	
	return -1
end

-- Returns the string class name for classID, the one that isn't all caps
function OpenGF_Util.GetClassName(classID)
	for k, v in pairs(OpenGF.classes) do
		if (v.id == classID) then 
			return v.name
		end
	end
	return "No Class"
end


function GetChannelIDFromName(channelName)
	return tonumber(string.match(string.sub(channelName,0, 1), "%d+"))
end
