---------------------------------------------------------------------
-- OpenGF_MainFrame
---------------------------------------------------------------------

OpenGF_MainFrame = {}

-- is the frame visible and interactable?
OpenGF_MainFrame.enabled = true

-- List of players in PlayerList
OpenGF_MainFrame.listedPlayers = {}
local listedPlayerCount = 0
OpenGF_MainFrame.currentCategory = nil

-- Maximum characters allowed in note
MAX_NOTE_SIZE = 46

-- Activates or disabled the main window and all its children.
-- Sets both visibility and intractable
function OpenGF_MainFrame.Toggle()
	
	if(OpenGF_MainFrame.enabled) then
		OpenGF_MainFrame.enabled = false
		OpenGF_MainFrame.frame:Hide()
	else
		OpenGF_MainFrame.enabled = true
		OpenGF_MainFrame.frame:Show()
	end
	
	-- Enabled mouse/keyboard interaction with frame depending on its visibility
	OpenGF_MainFrame.frame:EnableMouse(OpenGF_MainFrame.enabled)
	OpenGF_MainFrame.frame:EnableKeyboard(OpenGF_MainFrame.enabled)

end

-- Gets the identifier for the selected category ("D" for dungeon, "H" for heroic etc.)
function OpenGF_MainFrame.GetCategoryIdentifier()
	
	for k, v in pairs(OpenGF.categories) do
		if(v.id == OpenGF_MainFrame.currentCategory - 1) then
			return v.identifier
		end
	end
	
	return nil
end

-- Updates which instance dropdowns should be visible, based on the selected category
function OpenGF_MainFrame.UpdateDropdowns(catID )
	
	-- Hide all dropdowns
	OpenGF_MainFrame.activeInstance:Hide()
	
	if (catID == 1) then
		OpenGF_MainFrame.activeInstance = OpenGF_MainFrame.vanInstances
	elseif (catID == 2) then
		OpenGF_MainFrame.activeInstance = OpenGF_MainFrame.tbcInstances
	elseif (catID == 3) then
		OpenGF_MainFrame.activeInstance = OpenGF_MainFrame.tbcHeroic
	elseif(catID == 4) then
		OpenGF_MainFrame.activeInstance = OpenGF_MainFrame.wotlkInstances
	elseif(catID == 5) then
		OpenGF_MainFrame.activeInstance = OpenGF_MainFrame.wotlkHeroic
	end
	
	-- Make sure the selected instance is 0, so the player is not queuing for the previously selected instance
	UIDropDownMenu_SetSelectedID(OpenGF_MainFrame.activeInstance, 0)
	UIDropDownMenu_SetText(OpenGF_MainFrame.activeInstance, "Select Instance")
	
	
	OpenGF_MainFrame.activeInstance:Show()
	
end
-- Hides all listed players and resets couint to zero for frame re-use
function OpenGF_MainFrame.ResetListedPlayers()
	
	if(listedPlayerCount > 0) then
		for i = 1 , listedPlayerCount , 1 do   
			OpenGF_MainFrame.listedPlayers[i]:Hide()
		end
		
		listedPlayerCount = 0
	end

end

-- Adds a player to the player list
function OpenGF_MainFrame.AddPlayerToList(plyName, data)
	
		listedPlayerCount = listedPlayerCount + 1
	
		local _, lastIndex, groupType, instId, plyLevel, plyClass, plyRole = string.find(data, "(%a+)%s*(%d+)%s*(%d+)%s*(%d+)%s*(%d+)%s*")
		
		local plyNote = string.sub(data, lastIndex + 1, string.len(data))

		--TODO highlights for rows
		
		if(tonumber(instId) == OpenGF.selectedInstance) then
			local height = 25
			-- Create the row if the nth row does not exist
			if(OpenGF_MainFrame.listedPlayers[listedPlayerCount] == nil) then
				OpenGF_MainFrame.listedPlayers[listedPlayerCount] = OpenGF_GUI.CreateListRow("listedPlayer_"..listedPlayerCount, OpenGF_MainFrame.playerList.scrollChild, 600, height, 1, - (4 + (height * (listedPlayerCount - 1))))
			end
			
			OpenGF_GUI.SetRowData(OpenGF_MainFrame.listedPlayers[listedPlayerCount], tonumber(plyLevel), tonumber(plyRole), plyName, tonumber(plyClass), plyNote, groupType)
			
			OpenGF_MainFrame.listedPlayers[listedPlayerCount]:Show()
		end

end

-- Hides or shows the search button, if the selected instance is valid
function OpenGF_MainFrame.UpdateSearchButton()
	if(OpenGF.selectedInstance ~= 0 and OpenGF.selectedInstance ~= nil) then
		OpenGF_MainFrame.searchButton:Show()
	else
		OpenGF_MainFrame.searchButton:Hide()
	end
end

-- Shows enlist, if unlist is active, visa vera
function OpenGF_MainFrame.UpdateListingMethod(instID)
	
	OpenGF_MainFrame.enlistButton:Hide()
	OpenGF_MainFrame.unlistButton:Hide()
	OpenGF_MainFrame.unlistAllButton:Hide()
	
	if(OpenGF_Util.GetListLength(OpenGF.listings) > 0) then
		OpenGF_MainFrame.unlistAllButton:Show()
	end

	if(instID > 0) then
		if(OpenGF_Util.ListContains(OpenGF.listings, instID)) then
			OpenGF_MainFrame.unlistButton:Show()
		else
			OpenGF_MainFrame.enlistButton:Show()
		end
	end
end

-- Creates the main addon window and hides it
function OpenGF_MainFrame.CreateWindow()

	-- The main frame frame
	OpenGF_MainFrame.frame = OpenGF_GUI.CreateFrame("OpenGF_MainFrame", UIParent, 640, 480, true )
	OpenGF_GUI.SetFrameStyle(OpenGF_MainFrame.frame, 0, 0, 0, 0.7)
	OpenGF_MainFrame.title = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "addonTitle", "Open Group Finder", 10, -10)
	
	-- Set up minimize button
	local buttonTexture= "Interface/Buttons/UI-Panel-MinimizeButton"
	OpenGF_MainFrame.minimize = OpenGF_GUI.CreateButton("minimizeButton", OpenGF_MainFrame.frame, 32, 32, nil, OpenGF_MainFrame.Toggle, 0, 0, nil, "TOPRIGHT", "TOPRIGHT" )
	OpenGF_GUI.SetButtonStyle(OpenGF_MainFrame.minimize, buttonTexture .. "-Up", buttonTexture .. "-Highlight", buttonTexture .. "-Down")

	-- Hide the OpenGF_MainFrame by default
	OpenGF_MainFrame.Toggle()
end

function OpenGF_MainFrame.CreateWidgets()

	-- Role checks:
	OpenGF_MainFrame.roleTag = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "roleLabel", "Select Role:", 200, -20)
	
	-- DPS
	OpenGF_MainFrame.dpsCheck = OpenGF_GUI.CreateCheckbox("dpsCheck", OpenGF_MainFrame.frame, 16, 18, OpenGF.UpdateRole, 200, -42)
	OpenGF_MainFrame.dpsTag = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "dpsLabel", "DPS", 220, -45)	
	-- TANK
	OpenGF_MainFrame.tankCheck = OpenGF_GUI.CreateCheckbox("tankCheck", OpenGF_MainFrame.frame, 16, 18, OpenGF.UpdateRole, 250, -42)
	OpenGF_MainFrame.tankTag = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "tankLabel", "Tank", 270, -45)
	-- HEALS
	OpenGF_MainFrame.healCheck = OpenGF_GUI.CreateCheckbox("healCheck", OpenGF_MainFrame.frame, 16, 18, OpenGF.UpdateRole, 305, -42)
	OpenGF_MainFrame.healTag = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "healLabel", "Heals", 325, -45)
	
	-- Note text field:
	OpenGF_MainFrame.noteTag = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "noteLabel", "Add Note:", 370, -20)
	OpenGF_MainFrame.note = OpenGF_GUI.CreateTextField("noteText", OpenGF_MainFrame.frame, 200, 24, false, -65, -40, "TOPRIGHT", "TOPRIGHT")
	-- Update the note when the cursor leaves the text area
	OpenGF_MainFrame.note:SetScript("OnLeave", OpenGF.UpdateNote)
		-- Set the max message size
	OpenGF_MainFrame.note:SetMaxBytes(MAX_NOTE_SIZE)
	-- Set the note text to anything that has been saved
	OpenGF_MainFrame.note:SetText(OpenGF_Data.Note)
	
	-- category selection:
	OpenGF_MainFrame.categoryLabel = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "categoryTag", "Category:", 30, -80)
	OpenGF_MainFrame.categorySelect = OpenGF_GUI.CreateDropdown("categorySelect", OpenGF_MainFrame.frame, 150, OpenGF.categories, 1, NUM_CATEGORIES, OpenGF.UpdateCategory, "Select category", 1, 10, -100)
	
	-- Instance selection:
	
	OpenGF_MainFrame.vanInstances = OpenGF_GUI.CreateDropdown("vanInstances", OpenGF_MainFrame.frame, 150, OpenGF.instances, VAN_INSTANCE_START , TBC_INSTANCE_START - 1, OpenGF.UpdateInstance, "Select Instance", 0, 185, -100)
		
	-- Set the current category to vanilla by default
	OpenGF_MainFrame.currentCategory = 1;
	-- and the OpenGF.instances to vanilla
	OpenGF_MainFrame.activeInstance = OpenGF_MainFrame.vanInstances
	
	OpenGF_MainFrame.tbcInstances = OpenGF_GUI.CreateDropdown("tbcInstances", OpenGF_MainFrame.frame, 150, OpenGF.instances, TBC_INSTANCE_START, TBCH_INSTANCE_START  - 1, OpenGF.UpdateInstance, "Select Instance", 0, 185, -100)
	OpenGF_MainFrame.tbcInstances:Hide();
	
	OpenGF_MainFrame.tbcHeroic = OpenGF_GUI.CreateDropdown("tbcHeroic", OpenGF_MainFrame.frame, 150, OpenGF.instances, TBCH_INSTANCE_START, WOTLK_INSTANCE_START  - 1, OpenGF.UpdateInstance, "Select Instance", 0, 185, -100)
	OpenGF_MainFrame.tbcHeroic:Hide();
	
	OpenGF_MainFrame.wotlkInstances = OpenGF_GUI.CreateDropdown("wotlkInstances", OpenGF_MainFrame.frame, 150, OpenGF.instances, WOTLK_INSTANCE_START, WOTLKH_INSTANCE_START - 1, OpenGF.UpdateInstance, "Select Instance", 0, 185, -100)
	OpenGF_MainFrame.wotlkInstances:Hide();
	
	OpenGF_MainFrame.wotlkHeroic = OpenGF_GUI.CreateDropdown("wotlkHeroic", OpenGF_MainFrame.frame, 150, OpenGF.instances, WOTLKH_INSTANCE_START, 93, OpenGF.UpdateInstance, "Select Instance", 0, 185, -100)
	OpenGF_MainFrame.wotlkHeroic:Hide();
	
	-- Search button
	OpenGF_MainFrame.searchButton = OpenGF_GUI.CreateButton("searchButton", OpenGF_MainFrame.frame, 76, 24, "Search", OpenGF.SearchFunction, -190, -102, "UIPanelButtonTemplate", "TOPRIGHT", "TOPRIGHT")
	OpenGF_MainFrame.searchButton:Hide()
	
	-- Enlist Button
	OpenGF_MainFrame.enlistButton = OpenGF_GUI.CreateButton("enlistButton", OpenGF_MainFrame.frame, 86, 24, "List Myself", OpenGF.EnlistFunction, -100, -102, "UIPanelButtonTemplate", "TOPRIGHT", "TOPRIGHT")
	OpenGF_MainFrame.enlistButton:Hide()
	
	-- Unlist button
	OpenGF_MainFrame.unlistButton = OpenGF_GUI.CreateButton("unlistButton", OpenGF_MainFrame.frame, 86, 24, "Unlist", OpenGF.UnlistFunction, -100, -102, "UIPanelButtonTemplate", "TOPRIGHT", "TOPRIGHT")
	OpenGF_MainFrame.unlistButton:Hide()
	
	-- Unlist All button
	OpenGF_MainFrame.unlistAllButton = OpenGF_GUI.CreateButton("unlistAllButton", OpenGF_MainFrame.frame, 86, 24, "Unlist All", OpenGF.UnlistAll, -10, -102, "UIPanelButtonTemplate", "TOPRIGHT", "TOPRIGHT")
	OpenGF_MainFrame.unlistAllButton:Hide()
	
	-- Player list 
	
	--header
	
	OpenGF_MainFrame.levelHeader = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "levelHeader", "Lvl", 30, -135)
	OpenGF_MainFrame.roleHeader = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "roleHeader", "Role", 60, -135)
	OpenGF_MainFrame.nameHeader = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "nameHeader", "Name", 107, -135)
	OpenGF_MainFrame.classHeader = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "classHeader", "Class", 250, -135)
	OpenGF_MainFrame.noteHeader = OpenGF_GUI.CreateLabel(OpenGF_MainFrame.frame, "noteHeader", "Note", 335, -135)
	
	-- Main playerList frame
	OpenGF_MainFrame.playerList = OpenGF_GUI.CreateFrame("playerList", OpenGF_MainFrame.frame, 600, 310, false, nil, "CENTER", 0, -65)
	OpenGF_GUI.SetFrameStyle(OpenGF_MainFrame.playerList, 0, 0, 0, 1)
	
	OpenGF_MainFrame.playerList.scrollFrame = CreateFrame("ScrollFrame", "scrollFrame", OpenGF_MainFrame.playerList, "UIPanelScrollFrameTemplate" )
	OpenGF_MainFrame.playerList.scrollFrame:SetHeight(OpenGF_MainFrame.playerList:GetHeight() - 8)
	OpenGF_MainFrame.playerList.scrollFrame:SetWidth(OpenGF_MainFrame.playerList:GetWidth() - 30)
	OpenGF_MainFrame.playerList.scrollFrame:SetPoint("TOPLEFT", OpenGF_MainFrame.playerList, "TOPLEFT", 4, -4)
	
	OpenGF_MainFrame.playerList.scrollChild = CreateFrame("Frame", "scrollChild", OpenGF_MainFrame.playerList.scrollFrame)
	OpenGF_MainFrame.playerList.scrollChild:SetHeight(OpenGF_MainFrame.playerList.scrollFrame:GetHeight())
	OpenGF_MainFrame.playerList.scrollChild:SetWidth(OpenGF_MainFrame.playerList.scrollFrame:GetWidth())
	OpenGF_MainFrame.playerList.scrollChild:SetPoint("TOPLEFT", OpenGF_MainFrame.playerList, "TOPLEFT", 0, 0)
	OpenGF_MainFrame.playerList.scrollFrame:SetScrollChild(OpenGF_MainFrame.playerList.scrollChild)
	
end

---------------------------------------------------------------------
-- OpenGF_PlayerContextMenu
---------------------------------------------------------------------

OpenGF_PlayerContextMenu = {}
OpenGF_PlayerContextMenu.visible = true

-- The player the menu actions will be interacting with
OpenGF_PlayerContextMenu.activePlayer = nil

-- displays the context menu for the player list
function OpenGF_PlayerContextMenu.Show(playerName, groupType)
	-- only show the menu if the player name is not nil
	if(playerName ~= nil) then
		OpenGF_PlayerContextMenu.activePlayer = playerName
		OpenGF_Player.dungeonGroupType = groupType
		OpenGF_PlayerContextMenu.visible  = true
		OpenGF_PlayerContextMenu.menu:Show()
		OpenGF_PlayerContextMenu.menu:EnableMouse(OpenGF_PlayerContextMenu.visible)
		
		local offset = 20
		
		local scale, mouseX, mouseY =OpenGF_PlayerContextMenu.menu:GetEffectiveScale(), GetCursorPosition()
		OpenGF_PlayerContextMenu.menu:SetPoint("TOPLEFT", nil, "BOTTOMLEFT", mouseX/scale - offset, mouseY/scale + offset)
	end
end

-- hides the context menu, removes player from memory
function OpenGF_PlayerContextMenu.Hide()

	OpenGF_PlayerContextMenu.activePlayer = nil
	OpenGF_PlayerContextMenu.visible  = false
	OpenGF_PlayerContextMenu.menu:Hide()
	OpenGF_PlayerContextMenu.menu:EnableMouse(OpenGF_PlayerContextMenu.visible)
end

-- Opens a whisper window to activePlayer
function OpenGF_PlayerContextMenu.Whisper()
	local window = ChatEdit_ChooseBoxForSend()
	window:SetAttribute("chatType", "WHISPER")
	window:SetAttribute("tellTarget", OpenGF_PlayerContextMenu.activePlayer)
	if (window ~= ChatEdit_GetActiveWindow())then 
		ChatFrame_OpenChat("") 
	end
	
	OpenGF_PlayerContextMenu.Hide()
end

-- Sends a group invite to activePlayer
function OpenGF_PlayerContextMenu.GroupInvite()
	
	OpenGF_Player.isDungeonGroup = true
	
	InviteUnit(OpenGF_PlayerContextMenu.activePlayer)
	
	OpenGF_PlayerContextMenu.Hide()
end

-- Creates a context menu for interacting with listed players
function OpenGF_PlayerContextMenu.Create()
	OpenGF_PlayerContextMenu.menu = OpenGF_GUI.CreateContextMenu("plyContextMenu", 120, 75)
	
	-- Make the frame dissapear if the cursor leaves the context menu
	OpenGF_PlayerContextMenu.menu:SetScript("OnLeave", function() OpenGF_PlayerContextMenu.Hide() end)
	
	-- TODO make menu options dynamically added
	--TEMP
	
	OpenGF_PlayerContextMenu.whisper = OpenGF_GUI.CreateContextItem("context_whisper", OpenGF_PlayerContextMenu.menu, 100, 20, "Whisper", OpenGF_PlayerContextMenu.Whisper, 10, -12, "TOPLEFT", "TOPLEFT")
	
	OpenGF_PlayerContextMenu.invite = OpenGF_GUI.CreateContextItem("context_invite", OpenGF_PlayerContextMenu.menu, 100, 20, "Group Invite", OpenGF_PlayerContextMenu.GroupInvite, 10, -31 ,"TOPLEFT", "TOPLEFT")
	
	OpenGF_PlayerContextMenu.cancel = OpenGF_GUI.CreateContextItem("context_cancel", OpenGF_PlayerContextMenu.menu, 100, 20, "Cancel", OpenGF_PlayerContextMenu.Hide, 10, -50 ,"TOPLEFT", "TOPLEFT")
	
	OpenGF_PlayerContextMenu.Hide()
end

---------------------------------------------------------------------
-- OpenGF_MinimapIcon
---------------------------------------------------------------------

OpenGF_MinimapIcon = {}

-- Repositions the minimap icon to the angle saved in OpenGF_Settings
function OpenGF_MinimapIcon.Reposition()
	OpenGF_MinimapIcon.frame:SetPoint("TOPLEFT","Minimap","TOPLEFT",52-(80*cos(						OpenGF_Settings.MinimapPos)),(80*sin(OpenGF_Settings.MinimapPos))-51)
end

-- Called per frame when MinimapDrag frame is visible
function OpenGF_MinimapIcon.OnUpdate()
	local xPos,yPos = GetCursorPosition()
	local xMin, yMin = Minimap:GetLeft(), Minimap:GetBottom()

	xPos = xMin-xPos/UIParent:GetScale()+70
	yPos = yPos/UIParent:GetScale()-yMin-70

	OpenGF_Settings.MinimapPos = math.deg(math.atan2(yPos,xPos))
	OpenGF_MinimapIcon.Reposition()
end

-- Update the minimap icon based on if the player is listed or not
function OpenGF_MinimapIcon.UpdateIcon()
	if(OpenGF_Util.GetListLength(OpenGF.listings) > 0) then
		OpenGF_MinimapIcon.bg:SetTexture(ICON_LISTED)
	else
		OpenGF_MinimapIcon.bg:SetTexture(ICON_UNLISTED)
	end
end

-- When the minimap icon is clicked
function OpenGF_MinimapIcon.OnClick()
	OpenGF_MainFrame.Toggle()
end

-- When the minimap icon drag is started
function OpenGF_MinimapIcon.OnDragStart()
	OpenGF_MinimapIcon.frame:LockHighlight()
	OpenGF_MinimapIcon.dragFrame:Show()
end

-- When the minimap icon drag has ended
function OpenGF_MinimapIcon.OnDragEnd()
	OpenGF_MinimapIcon.frame:UnlockHighlight()
	OpenGF_MinimapIcon.dragFrame:Hide()
end

-- Creates a minimap icon that can be dragged and pressed. imgName is the path to the background image. Make sure to escape strings!
function OpenGF_MinimapIcon.Create(imgName)
	-- Create the main clickable frame
	OpenGF_MinimapIcon.frame = CreateFrame("Button", "OpenGF_OpenGF_MinimapIcon", Minimap)
	OpenGF_MinimapIcon.frame:EnableMouse(true)
	OpenGF_MinimapIcon.frame:SetMovable(true)
	OpenGF_MinimapIcon.frame:SetWidth(33)
	OpenGF_MinimapIcon.frame:SetHeight(33)
	OpenGF_MinimapIcon.frame:SetPoint("TOPLEFT", -9, 0)
	OpenGF_MinimapIcon.frame:SetFrameStrata("LOW")
	OpenGF_MinimapIcon.frame:SetHighlightTexture("Interface\\Minimap\\UI-Minimap-ZoomButton-Highlight", "ADD")
	
	-- Create the background texture
	OpenGF_MinimapIcon.bg = OpenGF_MinimapIcon.frame:CreateTexture("OpenGF_MinimapIcon_Texure", "BACKGROUND")
	OpenGF_MinimapIcon.bg:SetTexture(imgName)
	OpenGF_MinimapIcon.bg:SetSize(21, 21)
	OpenGF_MinimapIcon.bg:SetPoint("CENTER")
	
	-- Create the overlay that covers the square texture with a nice rounded frame
	OpenGF_MinimapIcon.overlay = OpenGF_MinimapIcon.frame:CreateTexture("OpenGF_MinimapIcon_Overlay", "OVERLAY")
	OpenGF_MinimapIcon.overlay:SetTexture("Interface\\Minimap\\MiniMap-TrackingBorder")
	OpenGF_MinimapIcon.overlay:SetSize(54, 54)
	OpenGF_MinimapIcon.overlay:SetPoint("TOPLEFT", 1, -1)
	
	-- Creates a frame that calls Minimap.OnUpdate, only when the frame is activated. The frame gets activated on drag start and deactivated on drag end
	OpenGF_MinimapIcon.dragFrame = CreateFrame("Frame", "OpenGF_MinimapIcon_DragFrame", OpenGF_MinimapIcon.frame)
	OpenGF_MinimapIcon.dragFrame:SetScript("OnUpdate", OpenGF_MinimapIcon.OnUpdate)
	OpenGF_MinimapIcon.dragFrame:Hide()
	
	-- Allow middle, right and left clicks for the main minimap frame
	OpenGF_MinimapIcon.frame:RegisterForClicks("LeftButtonUp", "RightButtonUp", "MiddleButtonUp")
	OpenGF_MinimapIcon.frame:RegisterForDrag("LeftButton", "RightButton")
	
	-- Set hooks
	OpenGF_MinimapIcon.frame:SetScript("OnDragStart", OpenGF_MinimapIcon.OnDragStart)
	OpenGF_MinimapIcon.frame:SetScript("OnDragStop", OpenGF_MinimapIcon.OnDragEnd)
	OpenGF_MinimapIcon.frame:SetScript("OnClick", OpenGF_MinimapIcon.OnClick)
	
end