## Interface: 30300
## Title: OpenGF
## Notes: Player matchmaking, so people can do group stuff together
## Version: 1.0
## Author: Pinecleandog
## SavedVariables: OpenGF_Settings
## SavedVariablesPerCharacter: OpenGF_Data
Util.lua
OpenGF_GUI.lua
OpenGF_Frames.lua
OpenGF_Core.lua
OpenGF_DB.lua
OpenGF_Comms.lua
OpenGF_Locale.lua
OpenGF_Player.lua
OpenGF.xml