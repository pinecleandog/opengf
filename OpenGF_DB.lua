ROLE_FLAG_DPS = 0x00000001
ROLE_FLAG_TANK = 0x00000002
ROLE_FLAG_HEAL= 0x00000004

GROUP_TYPE_DUNGEON = 'D'
GROUP_TYPE_HEROIC = 'H'
GROUP_TYPE_RAID = 'R'
GROUP_TYPE_CUSTOM = 'C'
GROUP_TYPE_BATTLEGROUND = 'B'

VAN_INSTANCE_START = 1
TBC_INSTANCE_START = 30
TBCH_INSTANCE_START = 46
WOTLK_INSTANCE_START = 62
WOTLKH_INSTANCE_START = 78

NUM_CATEGORIES = 5

-- Not actually categorys, just OpenGF.categories
OpenGF.categories =
{
	{id = 0, name = "Vanilla", identifier =  "D"},
	{id = 1, name = "The Burning Crusade",  identifier = "D"},
	{id = 2, name = "The Burning Crusade Heroic",  identifier = "H"},
	{id = 3, name = "Wrath of The Lich King",  identifier = "D"},
	{id = 4, name = "Wrath of The Lich King Heroic",  identifier = "H"},
}

OpenGF.classes = 
{
	{id = 0, name = "No Class"},
	{id = 1, name = "Warrior"},
	{id = 2, name = "Paladin" },
	{id = 3, name = "Hunter" },
	{id = 4, name = "Rogue" },
	{id = 5, name = "Priest"},
	{id = 6, name = "Deathknight"},
	{id = 7, name= "Shaman"},
	{id = 8, name = "Mage"},
	{id = 9, name = "Warlock"},
	{id = 10, name = "Druid"},
}

OpenGF.classRoles = 
{
	{id = 0, roles = {} },
	{id = 1, roles = {ROLE_FLAG_DPS, ROLE_FLAG_TANK}},
	{id = 2, roles = {ROLE_FLAG_DPS, ROLE_FLAG_TANK, ROLE_FLAG_HEAL} },
	{id = 3, roles = {ROLE_FLAG_DPS} },
	{id = 4, roles = {ROLE_FLAG_DPS} },
	{id = 5, roles = {ROLE_FLAG_DPS, ROLE_FLAG_HEAL}},
	{id = 6, roles = {ROLE_FLAG_DPS, ROLE_FLAG_TANK}},
	{id = 7, roles = {ROLE_FLAG_DPS, ROLE_FLAG_HEAL}},
	{id = 8, roles = {ROLE_FLAG_DPS}},
	{id = 9, roles = {ROLE_FLAG_DPS}},
	{id = 10, roles = {ROLE_FLAG_DPS, ROLE_FLAG_TANK, ROLE_FLAG_HEAL}},

}

OpenGF.instances = 
{
	-- VANILLA
	{id = 1, name = "Ragefire Chasm", minLevel = 15, maxLevel = 21, ilevel = 0},
	{id = 2, name = "The Deadmines", minLevel = 15, maxLevel = 25, ilevel = 0},
	{id = 3, name = "Wailing Caverns", minLevel = 15, maxLevel = 25, ilevel = 0},
	{id = 4, name = "Shadowfang Keep", minLevel = 16, maxLevel = 26, ilevel = 0},
	{id = 5, name = "Blackfathom Deeps", minLevel = 19, maxLevel = 29, ilevel = 0},
	{id = 6, name = "The Stockade", minLevel = 20, maxLevel = 30, ilevel = 0},
	{id = 7, name = "Razorfen Kraul", minLevel = 22, maxLevel = 32, ilevel = 0},
	{id = 8, name = "Gnomeregan", minLevel = 23, maxLevel = 33, ilevel = 0},
	{id = 9, name = "Scarlet Monastery - Graveyard", minLevel = 27, maxLevel = 37, ilevel = 0},
	{id = 10, name = "Scarlet Monastery - Library", minLevel = 30, maxLevel = 40, ilevel = 0},
	{id = 11, name = "Scarlet Monastery - Armory", minLevel = 32, maxLevel = 42, ilevel = 0},
	{id = 12, name = "Razorfen Downs", minLevel = 32, maxLevel = 42, ilevel = 0},
	{id = 13, name = "Scarlet Monastery - Cathedral", minLevel = 35, maxLevel = 45, ilevel = 0},
	{id = 14, name = "Uldaman", minLevel = 35, maxLevel = 45, ilevel = 0},
	{id = 15, name = "Zul'Farrak", minLevel = 41, maxLevel = 51, ilevel = 0},
	{id = 16, name = "Maraudon - Orange Crystals", minLevel = 41, maxLevel = 51, ilevel = 0},
	{id = 17, name = "Maraudon - Purple Crystals", minLevel = 41, maxLevel = 51, ilevel = 0},
	{id = 18, name = "Maraudon - Pristine Waters", minLevel = 43, maxLevel = 53, ilevel = 0},	
	{id = 19, name = "Blackrock Depths - Detention Block", minLevel = 47, maxLevel = 57, ilevel = 0},
	{id = 20, name = "Sunken Temple", minLevel = 45, maxLevel = 55, ilevel = 0},
	{id = 21, name = "Blackrock Depths - Upper City", minLevel = 51, maxLevel = 61, ilevel = 0},
	{id = 22, name = "Dire Maul - East", minLevel = 53, maxLevel = 63, ilevel = 0},
	{id = 23, name = "Scholomance", minLevel = 55, maxLevel = 65, ilevel = 0},
	{id = 24, name = "Dire Maul - North", minLevel = 55, maxLevel = 65, ilevel = 0},
	{id = 25, name = "Dire Maul - West", minLevel = 55, maxLevel = 65, ilevel = 0},
	{id = 26, name = "Stratholme - Main Gate", minLevel = 55, maxLevel = 65, ilevel = 0},
	{id = 27, name = "Stratholme - Service Entrance", minLevel = 55, maxLevel = 65, ilevel = 0},
	{id = 28, name = "Lower Blackrock Spire", minLevel = 55, maxLevel = 65, ilevel = 0},
	{id = 29, name = "Upper Blackrock Spire", minLevel = 55, maxLevel = 65, ilevel = 0},
	-- THE BURNING CRUSADE
	{id = 30, name = "Hellfire Citadel: Ramparts", minLevel = 57, maxLevel = 67, ilevel = 0},
	{id = 31, name = "Hellfire Citadel: The Blood Furnace", minLevel = 59, maxLevel = 68, ilevel = 0},
	{id = 32, name = "Coilfang: The Slave Pens", minLevel = 60, maxLevel = 69, ilevel = 0},
	{id = 33, name = "Coilfang: The Underbog", minLevel = 61, maxLevel = 70, ilevel = 0},
	{id = 34, name = "Auchindoun: Mana-Tombs", minLevel = 62, maxLevel = 70, ilevel = 0},
	{id = 35, name = "Auchindoun: Auchenai Crypts", minLevel = 63, maxLevel = 70, ilevel = 0},
	{id = 36, name = "The Escape from Durnholde", minLevel = 64, maxLevel = 70, ilevel = 0},
	{id = 37, name = "Auchindoun: Sethekk Halls", minLevel = 65, maxLevel = 70, ilevel = 0},
	{id = 38, name = "Auchindoun: Shadow Labyrinth", minLevel = 67, maxLevel = 70, ilevel = 0},
	{id = 39, name = "Hellfire Citadel: The Shattered Halls", minLevel = 67, maxLevel = 70, ilevel = 0},
	{id = 40, name = "Coilfang: The Steamvault", minLevel = 67, maxLevel = 70, ilevel = 0},
	{id = 41, name = "Tempest Keep: The Botanica", minLevel = 67, maxLevel = 70, ilevel = 0},
	{id = 42, name = "Tempest Keep: The Mechanar", minLevel = 67, maxLevel = 70, ilevel = 0},
	{id = 43, name = "Opening of the Dark Portal", minLevel = 68, maxLevel =70, ilevel = 0},
	{id = 44, name = "Tempest Keep: The Arcatraz", minLevel = 68, maxLevel = 70, ilevel = 0},
	{id = 45, name = "Magisters' Terrace", minLevel = 68, maxLevel = 70, ilevel = 0},
		-- THE BURNING HEROIC
	{id = 46, name = "Hellfire Citadel: Ramparts Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 47, name = "Hellfire Citadel: The Blood Furnace Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 48, name = "Coilfang: The Slave Pens Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 49, name = "Coilfang: The Underbog Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 50, name = "Auchindoun: Mana-Tombs Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 51, name = "Auchindoun: Auchenai Crypts Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 52, name = "The Escape from Durnholde Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 53, name = "Auchindoun: Sethekk Halls Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 54, name = "Auchindoun: Shadow Labyrinth Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 55, name = "Hellfire Citadel: The Shattered Halls Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 56, name = "Coilfang: The Steamvault Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 57, name = "Tempest Keep: The Botanica Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 58, name = "Tempest Keep: The Mechanar Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 59, name = "Opening of the Dark Portal Heroic", minLevel = 70, maxLevel =70, ilevel = 0},
	{id = 60, name = "Tempest Keep: The Arcatraz Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	{id = 61, name = "Magisters' Terrace Heroic", minLevel = 70, maxLevel = 70, ilevel = 0},
	-- WRATH OF THE LICH KING
	{id = 62, name = "Utgarde Keep", minLevel = 68, maxLevel = 80, ilevel = 0},
	{id = 63, name = "The Nexus", minLevel = 69, maxLevel = 80, ilevel = 0},
	{id = 64, name = "Azjol-Nerub", minLevel = 70, maxLevel = 80, ilevel = 0},
	{id = 65, name = "Ahn'kahet", minLevel = 71, maxLevel = 80, ilevel = 0},
	{id = 66, name = "Drak'Tharon Keep", minLevel = 72, maxLevel = 80, ilevel = 0},
	{id = 67, name = "Violet Hold", minLevel = 73, maxLevel = 80, ilevel = 0},
	{id = 68, name = "Gundrak", minLevel = 74, maxLevel = 80, ilevel = 0},
	{id = 69, name = "Halls of Stone", minLevel = 75, maxLevel = 80, ilevel = 0},
	{id = 70, name = "Halls of Lightning", minLevel = 77, maxLevel = 80, ilevel = 0},
	{id = 71, name = "The Culling of Stratholme", minLevel =77, maxLevel = 80, ilevel = 0},
	{id = 72, name = "The Oculus", minLevel = 77, maxLevel = 80, ilevel = 0},
	{id = 73, name = "Utgarde Pinnacle", minLevel = 77, maxLevel = 80, ilevel = 0},
	{id = 74, name = "Trial of the Champion", minLevel =78, maxLevel = 80, ilevel = 180},
	{id = 75, name = "Forge of Souls", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 76, name = "Pit of Saron", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 77, name = "Halls of Reflection", minLevel = 80, maxLevel = 80, ilevel = 180},
	-- WRATH OF THE LICH KING HEROIC
	{id = 78, name = "Utgarde Keep Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 79, name = "The Nexus Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 80, name = "Azjol-Nerub Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 81, name = "Ahn'kahet Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 82, name = "Drak'Tharon Keep Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 83, name = "Violet Hold Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 84, name = "Gundrak Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 85, name = "Halls of Stone Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 86, name = "Halls of Lightning Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 87, name = "The Culling of Stratholme Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 88, name = "The Oculus Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 89, name = "Utgarde Pinnacle Heroic", minLevel = 80, maxLevel = 80, ilevel = 180},
	{id = 90, name = "Trial of the Champion Heroic", minLevel = 80, maxLevel = 80, ilevel = 200},
	{id = 91, name = "Forge of Souls Heroic", minLevel = 80, maxLevel = 80, ilevel = 200},
	{id = 92, name = "Pit of Saron Heroic", minLevel = 80, maxLevel = 80, ilevel = 200},
	{id = 93, name = "Halls of Reflection Heroic", minLevel = 80, maxLevel = 80, ilevel = 219}
}