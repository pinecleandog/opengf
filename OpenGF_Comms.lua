OpenGF_Comms = {}

-- How long to delay searches
SEARCH_DELAY = 3

OpenGF_Comms.channel = "ogf"
OpenGF_Comms.prefix = "ogf"

-- Inits the addon and chat listener
function OpenGF_Comms.Init()
	OpenGF_Comms.addonListener = CreateFrame("Frame", "ogf_addonListener")
	OpenGF_Comms.addonListener:RegisterEvent("CHAT_MSG_ADDON")
	OpenGF_Comms.addonListener:SetScript("OnEvent", OpenGF_Comms.OnAddonMessage)
	
	OpenGF_Comms.chatListener = CreateFrame("Frame", "ogf_chatListener")
	OpenGF_Comms.chatListener:RegisterEvent("CHAT_MSG_CHANNEL")
	OpenGF_Comms.chatListener:SetScript("OnEvent", OpenGF_Comms.OnChatMessage)
	
	ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL", OpenGF_Comms.FilterChannel)
	
	-- Disable as many channel notices as we can
	ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL_NOTICE_USER", OpenGF_Comms.FilterNotice)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL_NOTICE", OpenGF_Comms.FilterNotice)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL_JOIN", OpenGF_Comms.FilterNotice)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL_LEAVE", OpenGF_Comms.FilterNotice)

	OpenGF_Comms.searchTimer = CreateFrame("Frame", "ogf_searchTimer")
end

-- Event handler for channel UI update, so the temporary channel can be made when all other channels are joined
channelUIListener = CreateFrame("Frame", "ogf_channelUIListener")
channelUIListener:RegisterEvent("CHANNEL_UI_UPDATE")

local searchTime = 0
local canSearch = true

-- Starts the delay between searches
function OpenGF_Comms.StartSearchTimer()
	OpenGF_MainFrame.searchButton:DisableDrawLayer("HIGHLIGHT")
	OpenGF_MainFrame.searchButton:SetButtonState("DISABLED", false)
	OpenGF_Comms.searchTimer:SetScript("OnUpdate", OpenGF_Comms.UpdateSearchTimer)
	searchTime = 0
	canSearch = false
end

-- Counts up to SEARCH_DELAY before enabling the next search
function OpenGF_Comms.UpdateSearchTimer(self, deltaTime)
	searchTime = searchTime + deltaTime
    if searchTime >= SEARCH_DELAY then
        OpenGF_MainFrame.searchButton:SetButtonState("NORMAL", false)
        canSearch = true
		-- disable the timer script
		OpenGF_Comms.searchTimer:SetScript("OnUpdate", nil)
    end
end

-- Filters channel notices for the OpenGF_Comms channel (join, leave, owner change etc)
function OpenGF_Comms.FilterNotice(a,b,event,plyName,e, channelName)
	id = GetChannelIDFromName(channelName)
	
	if(id == GetChannelName(OpenGF_Comms.channel)) then
		
		if(event == "SET_MODERATOR" or event == "OWNER_CHANGED") then
			if(plyName == UnitName("player")) then
				-- Set the channel's password to nothing, incase the last owner/mod had set a password
				SetChannelPassword(OpenGF_Comms.channel, "");
			end
		end
		
		-- You can never leave! hahahaha
		if(event == "YOU_LEFT") then
			-- re register event to force the player to rejoin the ogf channel
			channelUIListener:RegisterEvent("CHANNEL_UI_UPDATE")
			return false
		end
		return true
	end

	return false
end

-- Filters player mesages on the OpenGF_Comms channel
function OpenGF_Comms.FilterChannel(a,b,c,d,e,channelName)
	
	id = GetChannelIDFromName(channelName)
	
	if(id == GetChannelName(OpenGF_Comms.channel)) then
		return true
	end

	return false

end

-- Called when an addon message is recieved
function OpenGF_Comms.OnAddonMessage(self, event, prefix, msg, channel, sender)
	--  check if the message recieved is from this addon only
		if(prefix == OpenGF_Comms.prefix) then
			OpenGF_MainFrame.AddPlayerToList(sender, msg)
		end
end

-- Called whenever a channel messaged is recieved (eg global, custom channel, general etc.)
function OpenGF_Comms.OnChatMessage(self, event, msg, sender, channelString, target, flags, unknown, unknown1, channelNumber, unknown2, counter, OpenGF_GUId)
	-- if it is the current comm channel id
	if(channelNumber == GetChannelName(OpenGF_Comms.channel) ) then
		-- if the message belongs to this addon
		if(string.sub(msg, 0, string.len(OpenGF_Comms.prefix)) == OpenGF_Comms.prefix) then
			--TODO different types of queries
			
			-- TEMP get the first integer from the string, this should be the only interger, so I won't worry about substringing it
			local groupType = string.sub(msg, string.len(OpenGF_Comms.prefix) + 2, string.len(OpenGF_Comms.prefix) + 3)
			local instId = string.match(msg, "%d+")
			-- respond if they have the dungeon id in their set
			if(OpenGF_Util.ListContains(OpenGF.listings, tonumber(instId))) then
				OpenGF_Comms.RespondToQuery(sender, tonumber(instId), groupType)
			end
		end
	end
end

-- Attempts to set up the custom temporary channel, provided the other channels have loaded first
function SetupChannel(self, event)
	--TODO what if someone puts a password on the channel?
	-- TODO what if the player has hit max channels?
	 -- Has the player joined other channels?
	if(GetNumDisplayChannels() > 0) then
		 JoinTemporaryChannel(OpenGF_Comms.channel)
		 -- Make sure this does not get run more than once
		 self:UnregisterEvent("CHANNEL_UI_UPDATE")
	end
end
channelUIListener:SetScript("OnEvent", SetupChannel)

-- Broadcasts a query over OpenGF_Comms.Channel, to find out who is queued for the instance id
function OpenGF_Comms.Broadcast(instID, groupType)
	if(canSearch) then
		OpenGF_Comms.StartSearchTimer()
		--TODO different types of broadcasts
		channelID, channelName = GetChannelName(OpenGF_Comms.channel);
		local msg = OpenGF_Comms.prefix .. " " .. groupType .. " ".. tostring(instID)
		SendChatMessage(msg, "CHANNEL", nil, channelID);
	end
end

-- Generates a response to a player's query, if the player is queued for that instance. Includes information required for generating the list
function OpenGF_Comms.RespondToQuery(sender, instId, groupType)
	
	local level = UnitLevel("player");
	local class = OpenGF_Player.GetPlayerClassID()

	-- instance id, character level, class, roles, note
	local msg = groupType .. tostring(instId) .. " ".. tostring(level) .. " " .. tostring(class) .. " " .. tostring(OpenGF_Data.Role) ..  " " .. OpenGF_Data.Note
	
	-- Send a private addon message to the sender of the broadcast
	SendAddonMessage(OpenGF_Comms.prefix, msg, "WHISPER", sender)
end


